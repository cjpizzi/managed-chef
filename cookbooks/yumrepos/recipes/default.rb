#
# Cookbook Name:: yumrepos
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.
repos = data_bag("custom-yum-repositories")

repos.each do |repo|
  rep = data_bag_item("custom-yum-repositories", repo)
  yum_repository rep['id'] do
    baseurl rep['baseurl']
    gpgkey rep['gpgkey']
    description rep['name']
    action :create
  end
end